## Overview
A collection of scripts designed to speed up SOC analysis and increase efficiency of SOC analysts

## Requirements
 - Python 2.7
 - ipwhois library
 - A VirusTotal api key

## Features

 1. **Nslookup** - Quick device hostname/ip
 2. **Domain extractor** - Provide a text file of ioc's, it'll convert it
    into a csv ready for Splunk ingestion, great for large lists of
    IOC's.
 3. **URL sanitiser** - Sanitise urls for sending/saving
 4. **Reputation check** - Checks a URL or IP against VT, gives any found VT reports, checks if it is a tor exit node and checks against badip's for known malicious activity
 5. **Hash suite** - Generate a hash for a given file, provide a hash and check hash in VT, gives a summary and report if any hits
 6. **HIBP** - check if accounts have been seen in breaches
 7. **DNS Tools** - Reverse DNS, DNS lookup and whois
 8. **Email tools** - Get headers from emails
 9. **Settings** - check connectivity to VT, add/remove api keys

## Installation instructions

First either clone the repository or download the project from gitlab

install dependencies with:

    pip install -r requirements.txt
   
If pip is not present, install with:

     sudo easy_install pip

Your VirusTotal key needs to be added using the settings menu before use.